

$('document').ready(function () {


    $("#contact-us-form").on('submit', function (e) {
        e.preventDefault();
        grecaptcha.ready(function () {
            grecaptcha.execute('6Le9DuQbAAAAALnIR870QDemdjxnu7f1Q0Re2Qau', { action: 'submit' }).then(function (token) {
                $.ajax({
                    url: "submit.php",
                    method: "POST",
                    dataType: 'json',
                    data: $("#contact-us-form").serialize(),
                    success: function (data) {
                        console.log(data);
                        console.log(data.status);

                        if (data.status == "1") {
                            $('.u-form-send-message.u-form-send-success').show();
                            setTimeout(function () {
                                $('.u-form-send-message.u-form-send-success').hide(1000);
                            }, 1000);
                        } else {
                            $('.u-form-send-message.u-form-send-message').show();
                            setTimeout(function () {
                                $('.u-form-send-message.u-form-send-message').hide(1000);
                            }, 1000);
                        }
                    },
                });
            });
        });


    });
});