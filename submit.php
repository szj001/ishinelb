<?php
header('Content-Type: application/json');
$to = "info@ishinelb.com";

$name = $_POST["name"] ?? null;
$email = $_POST["email"] ?? null;
$message = $_POST["message"] ?? null;

if ($name == null || $email == null || $message == null) {
    echo json_encode(["status" => "0", "message" => "fields are required"]);
    return;
}
$mail = mail($to, "Webiste Contact Form ($email)", "New Message from $name: \n $message");
if ($mail) {
    echo json_encode(["status" => "1", "message" => "email sent"]);
} else {
    echo json_encode(["status" => "0", "message" => "error while sending email"]);
}
